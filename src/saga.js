import { takeEvery, put, call, all } from 'redux-saga/effects'
// import { delay } from 'redux-saga/effects'
import GetAuthors from './helpers/fetchAuthorsApi'
import GetWordsPerAuthor from './helpers/fetchAuthorWordsApi'
import GetGlobalWords from './helpers/fetchGlobalWordsApi'

// authors update generator functions

function* AuthorsUpdateAsync()
{
    let authors = yield GetAuthors();
    const authorsList = Object.keys(authors).map((key, i) => {
        return { id: i, active: false, name: authors[key], alias: key, words: {}};
    });

    yield console.log("Remaped authors: ", authorsList);
    yield put({ type: 'authors/update', payload: authorsList });
};

function* WatchAuthorsUpdate()
{
    yield takeEvery('authors/update-async', AuthorsUpdateAsync);
};

// update words per author generator functions

function* WordListPerAuthorUpdate(action)
{
    let words = yield GetWordsPerAuthor(action.payload.alias);
    yield put({type: 'authors/fetchforselected', wordsList: words, index: action.payload.id});
}

function* WatchWordListPerAuthorUpdate()
{
    yield takeEvery('authors/fetchforselected-async', WordListPerAuthorUpdate);
}

// update global words list

function* GlobalWordsListUpdate()
{
    let words = yield GetGlobalWords();
    yield put({ type: 'stats/update', wordsList: words });
}

function* WatchGlobalWordsListUpdate()
{
    yield takeEvery('stats/update-async', GlobalWordsListUpdate);
}

// watchers array

export function* SagaWatchers()
{
    yield all([
        call(WatchAuthorsUpdate),
        call(WatchWordListPerAuthorUpdate),
        call(WatchGlobalWordsListUpdate)
    ])
}