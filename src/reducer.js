import {combineReducers} from 'redux'
import authorsReducer from './features/authors/authorsReducer'
import allStatsReducer from './features/all_stats/allStatsReducer'

const rootReducer = combineReducers({
        authors: authorsReducer,
        stats: allStatsReducer
});

export default rootReducer;