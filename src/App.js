import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import RootComponent from './components/function/RootComponent'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/stats">
          <Stats/>
        </Route>
        <Route>
          <Home/>
        </Route>
      </Switch>
    </Router>
  );
}

function Stats()
{
  return (
    <div className="App">
      <RootComponent/>
    </div>
  )
}

function Home()
{
  return (
    <div>
      <h2>
        Home Page
      </h2>
      <Link to="/stats">
        redirect to statistics
      </Link>
    </div>
  )
}

export default App;
