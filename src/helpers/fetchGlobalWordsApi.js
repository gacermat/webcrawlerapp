import axios from 'axios'
import baseUrl from '../baseUrl'

const globalWordsApi = `${baseUrl}stats/`;

const GetGlobalWords = async () => {
    try {
        const response = await axios.get(globalWordsApi);
        const words = response.data;
        console.log("Fetched global words: ", words);
        return words; 
    }
    catch(e) {
        console.log("Unresolved error");
        return {"error": "error"};
    }
}

export default GetGlobalWords;