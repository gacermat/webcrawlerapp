import axios from 'axios'
import baseUrl from '../baseUrl'

const authorsApi = `${baseUrl}authors/`;

const GetAuthors = async () => {
    try {
        const response = await axios.get(authorsApi);
        const authors = response.data;
        console.log("Fetched authors: ", authors);
        return authors;
    }
    catch(e) {
        console.log("Unresolved error");
        return {"error": "error"};
    }
};

export default GetAuthors;