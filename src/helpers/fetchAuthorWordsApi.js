import axios from 'axios'
import baseUrl from '../baseUrl'

const baseAuthorWordsApi = `${baseUrl}stats/`;

const GetWordsPerAuthor = async (author) => {
    try {
        const response = await axios.get(`${baseAuthorWordsApi}${author}`);
        const words = response.data;
        console.log(`Fetched words for ${author} `, words);
        return words; 
    }
    catch(e) {
        console.log("Unresolved error");
        return {"error": "error"};
    }
}

export default GetWordsPerAuthor;