import React, {Component} from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ArrowRight from '@material-ui/icons/ArrowRight';
import StarBorder from '@material-ui/icons/StarBorder';
import Checkbox from '@material-ui/core/Checkbox';
import store from '../../store'
import { useSelector } from 'react-redux'
import { css } from "@emotion/core";
import BounceLoader from "react-spinners/ClipLoader";

const styles = makeStyles((decorate) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        // somehow theme object is not injected into props, bug?
        // backgroundColor: theme.pallete.background.paper,
        backgroundColor: '#bababa'
    },
    nested: {
      paddingLeft: "40px"
    },
}));

const selectAuthors = state => state.authors.list;
const selectStats = state => state.stats;

export default function AuthorsList(props)
{
  const classes = styles();
  const [open, setOpen] = React.useState(false);
  const [selectAll, setSelectAll] = React.useState(false);
  const authors = useSelector(selectAuthors);
  const statsSelect = useSelector(selectStats);

  let [loading, setLoading] = React.useState(true);
  let [color, setColor] = React.useState("#49494a");

  const handleClick = () => {
    store.dispatch({ type: 'authors/update-async' });
    setOpen(!open);
  };

  const handleSelect = (value) => {
    let newState = value;
    newState.active = !value.active;
    store.dispatch({ type: 'authors/fetchforselected-async', payload: newState });
    store.dispatch({ type: 'authors/setactive', payload: newState });
    store.dispatch({ type: 'stats/setactive', payload: false});
  };

  const handleSelectAll = () => {
    store.dispatch({ type: 'stats/update-async' });
    store.dispatch({ type: 'authors/setallactive', payload: false});
    store.dispatch({ type: 'stats/setactive', payload: !statsSelect.active})
  };

  return(
    <div>
      <List className={classes.root}>
        <ListItem button onClick={() => handleClick()}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Select Stats" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button key={-1} className={classes.nested} onClick={() => handleSelectAll()}>
              <ListItemIcon>
                <ArrowRight/>
              </ListItemIcon>
              <ListItemText primary={"All"}/>
              <Checkbox
                edge="start"
                checked={statsSelect.active}
                tabIndex={-1}
                disableRipple
              />
            </ListItem>
            { (Array.isArray(authors) && authors.length) ? ( authors.map((value) => {
              return (
                <ListItem button key={value.id} className={classes.nested} onClick={() => handleSelect(value)}>
                  <ListItemIcon>
                    <StarBorder />
                  </ListItemIcon>
                  <ListItemText primary={`${value.name}`}/>
                  <Checkbox
                    edge="start"
                    checked={value.active}
                    tabIndex={-1}
                    disableRipple
                  />
                </ListItem>
              );
            })) : (
                <ListItem button key={0} className={classes.nested}>
                  <ListItemText primary="loading..." />
                  <BounceLoader color={color} loading={loading} size={25} />
                </ListItem>
              ) 
            }
          </List>
        </Collapse>
      </List>
    </div>
  );
};