import React from 'react'
import AuthorsList from './AuthorsList'
import WordsTable from './WordsTable'

export default function RootComponent(props)
{
    return (
        <div>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
            <AuthorsList/>
            <WordsTable/>
        </div>
    )
}