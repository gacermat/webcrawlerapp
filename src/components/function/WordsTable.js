import React from 'react'
import { makeStyles, Theme } from '@material-ui/core/styles';
import store from '../../store'
import { useSelector } from 'react-redux'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = makeStyles({
  table: {
    minWidth: 100
  },
  container: {
      maxWidth: 200,
      backgroundColor: 'grey',
      marginTop: '2%'
  }
});

const selectAuthorsList = state => state.authors.list;
const selectAllStats = state => state.stats;

export default function WordsTable(props)
{
    const classes = styles();
    const authorsList = useSelector(selectAuthorsList);
    const statsDict = useSelector(selectAllStats);

    const resolveTableContent = () => {
      let concatWordDict = {};

      if (Object.keys(authorsList).length !== 0)
      {
        console.log(authorsList);
        authorsList.forEach((author) => {
          if (author.active)
          {
            Object.keys(author.words).forEach( key => {
              let foundKey = Object.keys(concatWordDict).find(k => k === key);
              if (foundKey)
              {
                console.log("Found existing key: " + foundKey);
                concatWordDict[foundKey] = concatWordDict[foundKey] + author.words[foundKey];
              }
              else
                concatWordDict[key] = author.words[key];
            });
          }
        });
      }
      
      if (Object.keys(concatWordDict).length === 0 && statsDict.active === true)
        concatWordDict = {...statsDict.words};

      if (concatWordDict != null)
      {
        let itemsOfWordDict = Object.keys(concatWordDict).map(key => {
          return [key, concatWordDict[key]];
        });

        itemsOfWordDict.sort(function(first, second) {
          return second[1] - first[1];
        });

        console.log(itemsOfWordDict);

        return (
          itemsOfWordDict.map(([wkey, wvalue], i) => {
            if (i < 10) // limiting display rows to 10
            {
              return (
              <TableRow key={wkey}>
                <TableCell align="left">{i+1}.</TableCell>
                <TableCell align="left">{wkey}</TableCell>
                <TableCell align="left">{wvalue}</TableCell>
              </TableRow>
              )
            }
          })
        )
      }
      else
        return [];
    };

    return (
      <TableContainer component={Paper} variant="outlined" className={classes.container}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">No.</TableCell>
              <TableCell align="left">Word</TableCell>
              <TableCell align="left">Count</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              resolveTableContent()
            }
          </TableBody>
        </Table>
      </TableContainer>
    )
}