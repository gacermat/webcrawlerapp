import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import Checkbox from '@material-ui/core/Checkbox';

const styles = decorate => ({
    root: {
        width: '100%',
        maxWidth: 360,
        // somehow theme object is not injected into props, bug?
        // backgroundColor: theme.pallete.background.paper,
        backgroundColor: '#bababa'
    },
    nested: {
      paddingLeft: "40px"
    },
});

class AuthorsList extends Component
{
    constructor()
    {
        super();
        this.state = {
          open: false,
          checked: []
        };
    }

    handleClick = () => {
      let openState = this.state.open;
      this.setState({open: !openState});
    };

    handleSelect = (value) => {
      const currentIndex = this.state.checked.indexOf(value);
      const newChecked = [...this.state.checked];

      if (currentIndex === -1) {
        newChecked.push(value);
      } else {
        newChecked.splice(currentIndex, 1);
      }

      this.setState({checked: newChecked});
    };

    render()
    {
        const {classes, theme} = this.props;

        return(
            <div>
              <List className={classes.root}>
                <ListItem button onClick={this.handleClick}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <ListItemText primary="Authors" />
                  {this.state.open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {[0, 1, 2, 3].map((value) => {
                      return (
                        <ListItem button key={value} className={classes.nested} onClick={() => this.handleSelect(value)}>
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText primary={`author_id_${value}`}/>
                          <Checkbox
                            edge="start"
                            checked={this.state.checked.indexOf(value) !== -1}
                            tabIndex={-1}
                            disableRipple
                            //inputProps={}
                          />
                        </ListItem>
                      );
                    })}
                  </List>
                </Collapse>
              </List>
            </div>
        )
    }
};

// dropping 'withTheme' option, no effect?
export default withStyles(styles)(AuthorsList);