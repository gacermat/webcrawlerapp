import React, {Component} from 'react';

export default class DefaultClass extends Component
{
    constructor(){
        super();
        this.state = {
            textAfterChange: "Change this text",
            alertText: "Text about to be changed"
        }
    }

    onClickTextChanged = () => {
        alert(this.state.alertText);
        this.setState({textAfterChange: "Text is changed"})
    }

    render()
    {
        return (
            <div>
                <h1>{this.state.textAfterChange}</h1>
                <button onClick = {this.onClickTextChanged} >Change Above Text</button>
            </div>
        )
    };
};