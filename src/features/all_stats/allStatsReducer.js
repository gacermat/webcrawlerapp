// const initialState = {
//     words: {"słowo_1": 12, "słowo_2": 32, "słowo_3": 43},
//     active: false
// };

const initialState = {
    words: {},
    active: false
};
  
export default function allStatsReducer(state = initialState, action) {
  switch (action.type) {
    case 'stats/update':
        {
            return {
                ...state,
                words: action.wordsList
            }
        }
    case 'stats/setactive':
    {
        return {
            ...state,
            active: action.payload
        }
    }
    default:
      return state
  }
}