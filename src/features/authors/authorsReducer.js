// const initialState = {
//     list: [
//       { id: 0, active: false, name: '<name>', alias: '<alias>', words: {"key_word": count}},
//     ]
// };

const initialState = {list: []};
  
export default function authorsReducer(state = initialState, action) {
  switch (action.type) {
    case 'authors/update':
        {
            return {
                ...state,
                list: action.payload
            }
        }
    case 'authors/setactive':
        {   
            if (Array.isArray(state.list) && state.list.length) {
                return {
                    ...state,
                    list: state.list.map(
                        (item, i) => i === action.payload.id ? {...item, active: action.payload.active}: item
                    )
                }
            }
            else
                return state;
        }
    case 'authors/setallactive':
        {
            if (Array.isArray(state.list) && state.list.length) {
                return {
                    ...state,
                    list: state.list.map(
                       (item) => {return {...item, active: action.payload}}
                    )
                }
            }
            else
                return state;
        }
    case 'authors/fetchforselected':
        {
            return {
                ...state,
                list: state.list.map(
                    (item) => {
                        if (item.id == action.index)
                            return {...item, words: action.wordsList};
                        else
                            return item;
                    }
                )
            }
        }
    default:
      return state
  }
}