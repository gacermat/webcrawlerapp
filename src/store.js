import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducer'
import createSagaMiddleware from 'redux-saga'
import { SagaWatchers } from './saga'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(SagaWatchers);

export default store;